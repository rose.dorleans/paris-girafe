// App.tsx

import './assets/style/style.css'

import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './pages/fragments/Header';
import Footer from './pages/fragments/Footer';
import Login from './pages/Login';
import Home from './pages/Home';
import useToken from "./services/auth/useToken";
import { useAuth } from "./services/auth/useAuth";


const App: React.FC = () => {
  const { token, setToken } = useToken();
  const { isConnected } = useAuth(token);
  console.log(isConnected)

  if(token.length <= 0 || !isConnected) {
    return <Router>
      <Header/>
          <Routes>
            <Route path="*" element={
              <Login setToken={setToken} />
            }/>
            </Routes>
        </Router>
  }

  return (
    <Router>
      <Header/>
      <Routes>
        <Route path="/" element={
            <Home />
        }/>
        <Route path="/home" element={
            <Home />
        }/>
        <Route path="/login" element={
            <Login />
        }/>
      </Routes>
    </Router>
  );
  

};

export default App;
