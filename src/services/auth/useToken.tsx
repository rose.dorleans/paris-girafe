import  { useState } from 'react';

export default function useToken() {
  const getToken = () => {
    const tokenString = localStorage.getItem('token');
    let userToken:string = "";
    if(tokenString)
     userToken = JSON.parse(tokenString)
    return userToken
  };
  const [token, setToken] = useState(getToken());

  const saveToken = (userToken:string) => {
    localStorage.setItem('token', JSON.stringify(userToken));
    setToken(userToken);
  };

  return {
    setToken: saveToken,
    token
  }
}