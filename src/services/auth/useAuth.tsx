import { useState } from "react";
import useToken from "./useToken";
import { useUser } from "./useUser";
import { User } from "../object/User";

export const useAuth = (token:string = "") => {
  const { setToken } = useToken();
  const [isConnected, setIsConnected]  = useState(true);
  const {user, setUser}  = useUser();

  const logout = () => {
    setToken("");
    setIsConnected(false)
  };

  const login = (user: User) => {
    setUser(user);
  };

  return { isConnected , user, logout, login};
};
