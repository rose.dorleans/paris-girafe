import { useState } from "react";
import { User } from "../object/User";

export const useUser = () => {
  const getUser = () => {
    const userString = localStorage.getItem('user');
    let user:User = { id: "", username: "", authToken: "" };
    if(userString)
    user = JSON.parse(userString)
    return user
  };
  const [ user, setUser ] = useState<User>(getUser());
  const saveUser = (user:User) => {
    localStorage.setItem('user', JSON.stringify(user));
    setUser(user);
  };
  return { user, setUser:saveUser };
};