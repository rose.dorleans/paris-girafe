import { useAuth } from "./auth/useAuth";

export const BASE_API_URL = "http://localhost/perso/paris-girafe-api/index.php/";

export interface ResponseProps{
    success : boolean,
    messages : {
        type : string,
        contentText : string,
        code : number,
    },
    data : any
}

export const Service_Api = () => {
    let token = "";
    if(localStorage.getItem("token") !== null) {
        token = JSON.parse(localStorage.getItem("token") ?? "");
    }
    const { logout } = useAuth(token);

    const request = async (options : any) => {
        let query ="";
        if(options.query){
            let first = true
            query += options.query.split("&").map((queryStr:any) => {
                if(first){
                    first = false;
                    return "?"+queryStr;
                }else{
                    return  "&"+queryStr;
                }
            })
        }else{
            query = new URLSearchParams(options.query || {}).toString();
            if (query !== '') {
                query = '?' + query;
            }
        }
        if(token)
            if(options.headers)
                options.headers.push({Authorization : token})
            else
                options.headers = {Authorization : token}
        let response;


        let api_resonse : ResponseProps;
        try{
            response = await fetch(BASE_API_URL + options.url + query, {
                method: options.method,
                headers: {
                    ...options.headers,
                },
                body: JSON.stringify(options.body),
            });
            api_resonse = await response?.json();
            
            if(api_resonse.messages.code === 1000){
                logout();
                window.location.reload();
            }

            return api_resonse;

        }catch(error){
            console.log(error)
            // logout();
            // window.location.reload();
        }
    }

    const get = async (url: string, query: string = "", options: {}= {}) => {
        return request({method: 'GET', url, query, ...options});
    }

    const post = async (url: string, body: {}= {}, options: {}= {}) => {
        return request({method: 'POST', url, body, ...options});
    }

    const put = async (url: string, body: {}= {}, options: {}= {}) => {
        return request({method: 'PUT', url, body, ...options});
    }

    const del = async (url: string, body: {}= {},  options: {}= {}) => {
        return request({method: 'DELETE', url, body, ...options});
    }
    return { get , post, put, del};
}


export default Service_Api;