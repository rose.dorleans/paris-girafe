export interface Bet {
    id: string;
    title: string;
    description: string;
    statutId: string;
    createdBy: string;
    publishedAt: string;
  }