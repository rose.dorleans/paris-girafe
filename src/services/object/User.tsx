export interface User {
    id: string;
    username: string;
    authToken?: string;
  }