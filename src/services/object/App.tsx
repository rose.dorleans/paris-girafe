export interface App {
    uid: string;
    name: string;
    url: string;
    icon: string;
  }
  