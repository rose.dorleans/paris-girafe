
import React, { useState, useEffect } from 'react';

import { Service_Api } from '../services/Api';
import { useAuth } from '../services/auth/useAuth';
import { Bet } from '../services/object/Bet';

import "../assets/style/style.css"

function Home() {
  const Api = Service_Api();
  const { user } = useAuth();
  console.log(user)

  const [TOBet, setTOBet] = useState<Bet[]>([])
  useEffect(() => {
    const fetchData = async () => {
      await fetchInitialData();
    };

    fetchData();
  }, []);

  const fetchInitialData = async () => {
    try {
      const response = await Api.get('bet/')
      setTOBet(response?.data)
      console.log('Données récupérées avec succès', response)
    } catch (error) {
      console.error('Erreur lors de la récupération des données')
    }
  };

  return (
    <div id='homePage' className='page-content'>
      <h2>Accueil</h2>
      <ul className="bet-list">
        {TOBet.map((bet, index) => (
          <li className="bet" key={index}>
            <h3>{bet.title}</h3>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Home;
