
import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";

import {Service_Api} from '../services/Api';
import { useAuth } from '../services/auth/useAuth';

const Login = ({setToken}:any) => {
  const Api = Service_Api();
  const { login } = useAuth();
  const navigate = useNavigate()
  
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleusernameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    let body = {
      username: username,
      password: password
    }
    let data = (await (Api.post('user/connexion', body, {})))?.data
    if(data){
        let user = {
            id: data.user.id,
            username: data.user.username,
            role: data.user.role,
            authToken: data.user.JWT
        };
        setToken(data.user.JWT);
        login(user);
        navigate('/home');
    }else{
        console.log(data)
    }
  };

  return (
    <div>
      <h2>Connexion</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="username">Pseudo:</label>
          <input
            type="username"
            id="username"
            value={username}
            onChange={handleusernameChange}
            required
          />
        </div>
        <div>
          <label htmlFor="password">Mot de passe:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={handlePasswordChange}
            required
          />
        </div>
        <button type="submit">Se connecter</button>
      </form>
    </div>
  );
};

export default Login;