
import React from 'react';
import iconGirafe from '../../assets/img/girafe.png'

const Header: React.FC = () => {
  return (
    <header>
       <h1>
        <span>Paris Girafe</span>
        <img src={iconGirafe} alt="logo" />
      </h1>
    </header>
  );
};

export default Header;
